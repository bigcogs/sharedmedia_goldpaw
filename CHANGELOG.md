# SharedMedia: Goldpaw Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/) 
and this project adheres to [Semantic Versioning](http://semver.org/).

## [1.0.2] 2017-08-29
### Changed
- Bumped the toc version to patch 7.3.0.

## [1.0.1] 2017-03-29
### Changed
- Bumped toc to patch 7.2.0.

### Fixed
- Fixed a bug that would keep LibSharedMedia-3.0 from being loaded by the addon.

