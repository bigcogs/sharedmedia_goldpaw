## Interface: 70300
## Name: SharedMedia_Goldpaw
## Title: SharedMedia: |cffff7d0aGoldpaw|r
## Version: 1.0
## Author: Lars Norberg
## Notes: Various media to match |cffff7d0aGoldpaw's UI|r.
## DefaultState: Enabled
## OptionalDeps: gUI4, LibSharedMedia-3.0
## X-Website: http://www.cogwerkz.org
## X-Donate: PayPal:paypal@cogwerkz.org
## X-License: The MIT License (MIT)
## X-Category: Artwork

### libraries ###
libs\LibStub\LibStub.lua
libs\CallbackHandler-1.0\CallbackHandler-1.0.lua
embeds.xml

### core ###
main.lua
